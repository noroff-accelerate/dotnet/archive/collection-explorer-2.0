﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Collection_Explorer_2_0
{
    class Program
    {
        //global input decision variables
        public static int populationChoice = 0, collectionChoice = 0;

        #region Global Collections
        public static List<string> listOfWords = new List<string>() {"Humdinger", "Supercilious", "Lilliputian", "Zamboni", "Jugulate", "Antidisestablishmentarianism", "Mellifluous", "Defenestrate", "Pulchritudinous", "Ecdysiast", "Discombobulated", "Sesquipedalianistic","Prestidigitation", "Unctuous", "Tumescent", "Pecksniffian", "pachycephalosaurus", "Duplicitous", "Duplicitous" };
        public static List<string> listOfWordsEmpty = new List<string>();
        public static ArrayList arrayListOfWords = new ArrayList();
        public static Queue<string> queueOfWords = new Queue<string>();
        public static Stack<string> stackOfWords = new Stack<string>();
        public static Dictionary<int, string> dictionaryOfWords = new Dictionary<int, string>();
        public static HashSet<string> hashSetOfWords = new HashSet<string>();
        #endregion

        static void Main(string[] args)
        {

            printHeader();

            //prompt loop
            do
            {

                prompt();

             } while (true);

            #region Examples of using methods

            //CountAsInList(listOfWords);

            //List<string> longWords = new List<string>();

            //for (int i = 0; i < 10; i++)
            //{
            //   longWords.Add(ConcatenateList(listOfWords));
            //}

            //Console.WriteLine(ConcatenateList(longWords));

            //List<string> longWords = new List<string>();


            //foreach (string aWord in GetListOfWordsWithAnA(listOfWords))
            //{
            //    Console.WriteLine(aWord);
            //}

            //int total = CountLetters(listOfWords[6], 'e') + CountLetters(listOfWords[6], 'p') + CountLetters(listOfWords[6], 'e');

            //Console.WriteLine(total);
            #endregion


        }

        public static void prompt()
        {
           
                printOptions();

                populationChoice = getIntegerInput(1,2);

                if (populationChoice == -1)
                {
                    Environment.Exit(-1);
                }

                printCollectionOptions();

                collectionChoice = getIntegerInput(1, 6);

                if (populationChoice == 1)
                {

                    switch (collectionChoice)
                    {
                        case 1:
                            populateFromInput(listOfWordsEmpty);
                            printCollection(listOfWordsEmpty);
                            break;

                        case 2:
                            populateFromInput(arrayListOfWords);
                            printCollection(arrayListOfWords);
                        break;

                        case 3:
                            populateFromInput(queueOfWords);
                            printCollection(queueOfWords);
                        break;

                        case 4:
                            populateFromInput(stackOfWords);
                            printCollection(stackOfWords);
                        break;

                        case 5:
                            populateFromInput(dictionaryOfWords);
                            printCollection(dictionaryOfWords);
                        break;

                        case 6:
                            populateFromInput(hashSetOfWords);
                            printCollection(hashSetOfWords);
                        break;

                        default:
                            Console.WriteLine("Choice invalid");
                            break;


                    }


             


                }


            #region NOTE
            /*
             The Switch statement below contains some statements that look like.... 
             
                  someCollection.ForEach(word => someOtherCollection.Add(word));

             This is the same as (is syntactic sugar for)......

                  foreach (string word in someCollection)
                  {
                      someOtherCollection.Add(word)
                  }
            */
            #endregion

                if (populationChoice == 2)
                {
                    Console.Clear();
                    

                    switch (collectionChoice)
                    {
                        case 1:
                        printCollection(listOfWords);
                            break;

                        case 2:
                        arrayListOfWords.AddRange(listOfWords);
                        printCollection(arrayListOfWords);
                            break;

                        case 3:
                        listOfWords.ForEach(word => queueOfWords.Enqueue(word));
                        printCollection(queueOfWords);
                            break;

                        case 4:
                        listOfWords.ForEach(word => stackOfWords.Push(word));
                        printCollection(stackOfWords);
                            break;

                        case 5:
                        int keyGen = 0;
                        listOfWords.ForEach(word => dictionaryOfWords.Add(keyGen++,word));
                        printCollection(dictionaryOfWords);
                            break;

                        case 6:
                        listOfWords.ForEach(word => hashSetOfWords.Add(word));
                        printCollection(hashSetOfWords);
                            break;

                    default:
                        Console.WriteLine("Choice invalid");
                            break;

                        
                    }

                }
            

        }

        #region Display Logic

        public static int add(params int[] arguments)
        {
            int result = 0;

            foreach (int num in arguments)
            {
                result += num;
            }

            return result;

        }


        public static void printHeader()
        {

            //header
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Welcome to the .NET Collection Explorer");
            Console.WriteLine("---------------------------------------");
        }

        public static void printOptions()
        {

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Please select from the following options by entering the number (Enter -1 to exit)" );
            Console.WriteLine("---------------------------------------");

            Console.WriteLine("1 - Populate Collection (Enter your own values)");

            Console.WriteLine("2 - Populated Collection (Already contains values)");

        }

        public static void printCollectionOptions()
        {

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Please select from the following options by entering the number");
            Console.WriteLine("---------------------------------------");

           
            Console.WriteLine("1 - List");

            Console.WriteLine("2 - ArrayList");

            Console.WriteLine("3 - Queue");

            Console.WriteLine("4 - Stack");

            Console.WriteLine("5 - Dictionary");

            Console.WriteLine("6 - HashSet");
        }

        public static void printCollection(ICollection<string> collection)
        {

            Console.WriteLine($"This {collection.GetType().Name} currently has {collection.Count} strings in it. They are listed below.");

            Console.WriteLine("---------------------------------------");

            foreach (string item in collection)
            {
                Console.WriteLine(item);
            }


        }

        public static void printCollection(ArrayList collection)
        {

            Console.WriteLine($"This {collection.GetType().Name} currently has {collection.Count} {collection[0].GetType().Name}s in it. They are listed below.");

            Console.WriteLine("---------------------------------------");

            foreach (string item in collection)
            {
                Console.WriteLine(item);
            }


        }

        public static void printCollection(Queue<string> collection)
        {

            Console.WriteLine($"This {collection.GetType().Name} currently has {collection.Count} {collection.Peek().GetType().Name}s in it. They are listed below.");

            Console.WriteLine("---------------------------------------");

            while (collection.Count != 0)
            {

                Console.WriteLine(collection.Dequeue());

            }

        }

        public static void printCollection(Stack<string> collection)
        {

            Console.WriteLine($"This {collection.GetType().Name} currently has {collection.Count} {collection.Peek().GetType().Name}s in it. They are listed below.");

            Console.WriteLine("---------------------------------------");

            while (collection.Count != 0)
            {

                Console.WriteLine(collection.Pop());

            }





        }

        public static void printCollection(Dictionary<int, string> collection)
        {

            Console.WriteLine($"This {collection.GetType().Name} currently has {collection.Count} {collection[0].GetType().Name} in it. They are listed below.");

            Console.WriteLine("---------------------------------------");



            foreach (var key in collection.Keys)
            {

                Console.WriteLine($"{key}: {collection[key]}");

            }


        }

        #endregion

        #region Input Logic

        public static int getIntegerInput(int min, int max)
        {
            Console.WriteLine("Please enter your choice:");

            var input = Console.ReadLine();
            int number;

           
            if (!int.TryParse(input,out number))
            {
                Console.WriteLine("Please enter your choice as a single number:");

                number = getIntegerInput(min,max);

            }

             if(number == -1)
                {

                    return number;

                }

            if (number >= min && number <= max)
            {

                return number;

            }
            else
            {

                Console.WriteLine($"Please make sure you input a valid option ({min} - {max}):");

                number = getIntegerInput(min, max);


            }


            return number;

        }

        public static string getStringInput()
        {
            Console.WriteLine("Please enter a word (Enter 'DONE' to submit the collection for display):");

            string input = Console.ReadLine();

            return input;


        }

        public static void populateFromInput(ICollection<string> collection)
        {
            string input;

            do
            {

                input = getStringInput();

                collection.Add(input);


            } while (input != "DONE");
            
            

        }

        public static void populateFromInput(ArrayList collection)
        {
            string input;

            do
            {

                input = getStringInput();

                collection.Add(input);


            } while (input != "DONE");



        }

        public static void populateFromInput(Queue<string> collection)
        {
            string input;

            do
            {

                input = getStringInput();

                collection.Enqueue(input);


            } while (input != "DONE");


        }

        public static void populateFromInput(Stack<string> collection)
        {
            string input;

            do
            {

                input = getStringInput();

                collection.Push(input);


            } while (input != "DONE");

        }

        public static void populateFromInput(Dictionary<int,string> collection)
        {
            string input;
            int key = 0;

            do
            {

                input = getStringInput();

                collection.Add(key++,input) ;


            } while (input != "DONE");



        }


        #endregion

        #region Additional Operations
        /* These are random methods to show possible parameter and return type options
         * */
        public static void CountAsInList(List<string> list)
        {
            //local variable
            int numberOfAs = 0;


            foreach (string word in list)
            {

                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == 'a' || word[i] == 'A')
                    {
                        numberOfAs++;
                    }
                }

            }

            Console.WriteLine($"There are:  {numberOfAs} a's in this list");
           
        }

        public static string ConcatenateList(List<string> list)
        {
            //local variable
            string longestWordEver = "";


            foreach (string word in list)
            {
                longestWordEver += word;
            }


            return longestWordEver;

        }

        public static List<string> GetListOfWordsWithAnA(List<string> list)
        {
            //local variable
            List<string> WordsWithAs = new List<string>();


            foreach (string word in list)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == 'a' || word[i] == 'A')
                    {
                        WordsWithAs.Add(word);
                        break;
                    }
                }

            }

            return WordsWithAs;

        }
        
        public static bool IsPossiblePolindrome(string word)
        {
            if (word.EndsWith(word[0]))
            {
                return true;
            }

            return false;

        }

        public static int CountLetters(string word, char letter)
        {
            //local variable
            int letterTotal = 0;

            for (int i = 0; i < word.Length; i++)
            {

                if (word[i] == letter)
                 {
                     letterTotal++;
                 }
                
            }

            return letterTotal;

        }


        #endregion
    }

}
