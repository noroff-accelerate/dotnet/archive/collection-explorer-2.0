# Collection Explorer 2.0

.NET Core Console Application

Intended for learning modularization as part of module 1 of Noroff Accelerate .NET Fullstack short course.

Demonstrates the use of variable scope, overloading, parameter options and the default behaviours of value types vs reference types


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




